Hey Rockstar!

Wanneer je het project opent en runt kun je met postman (of een andere tool) de API benaderen.

Als eerste wil je Artists en Songs toevoegen. Dit kan met de volgende URI's
(Jouw poortnummer krijg je te zien in de browser wanneer je het project runt)

localhost:[poort]/api/songs
localhost:[poort]/api/artists

Dit zijn beide POST requests. De body is in het zelfde format als de aangeleverde JSON bestanden en je moet nog een header toevoegen:
Content-Type:application/json

Tenzij je natuurlijk van XML output houdt dan hoeft dat niet.

Wanneer je de songs en artists hebt toegevoegd kun je met de zelfde URI's GET/PUT/DELETE request maken om de data de kunnen manipuleren. Een klein voorbeeld:

GET localhost:[poort]/api/songs
Verkrijgt alle songs

GET localhost:[poort]/api/songs/2
Verkijgt de song met ID 2

PUT localhost:[poort]/api/songs/2
Wanneer je in de body een song object meegeeft, dan wordt de data van de song met ID 2 aangepast

DELETE localhost:[poort]/api/songs/2
Verwijder de song met ID 2

Er is ook nog een zoekfuntie voor artists. Die benader je met de volgende URI:

GET localhost:[poort]/api/search/?input=rock&column=genre
Bij deze request geef je twee parameters mee. "input" is de waarde waarop je wilt zoeken en column is het veld waarop je wilt zoeken. Je kunt op 3 velden zoeken. 
- name
- year
- genre
Bij input mag je alles invullen wat je wilt.

Is dit genoeg informatie om door de API heen te komen? Zo niet mail dan even!
angelo_brouns@hotmail.com
