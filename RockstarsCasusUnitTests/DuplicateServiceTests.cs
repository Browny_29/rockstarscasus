﻿using System;
using RockstarsCasus.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RockstarsCasus.Models;
using System.Collections.Generic;
using System.Linq;

namespace RockstarsCasusUnitTests
{
    [TestClass]
    public class DuplicateServiceTests
    {
        [TestMethod]
        public void RemoveDuplicateSongs_GiveEmptyCollection_ExpectEmptyCollection()
        {
            var result = DuplicateService.RemoveDuplicateSongs(new List<Song>());

            Assert.IsTrue(result.Count == 0);
        }

        [TestMethod]
        public void RemoveDuplicateSongs_GiveFiveSameSongNames_ExpectFirstSong()
        {
            var songs = new List<Song> {
                new Song { Name = "Song" },
                new Song { Name = "Song" },
                new Song { Name = "Song" },
                new Song { Name = "Song" },
                new Song { Name = "Song" },
            };

            List<Song> result = DuplicateService.RemoveDuplicateSongs(songs).ToList();

            Assert.IsTrue(result.Count == 1);
            Assert.IsTrue(result[0].Name == songs[0].Name);
        }

        [TestMethod]
        public void RemoveDuplicateSongs_GiveFiveDifferentSongNames_ExpectFiveSongs()
        {
            var songs = new List<Song> {
                new Song { Name = "Song" },
                new Song { Name = "Song2" },
                new Song { Name = "Song3" },
                new Song { Name = "Song4" },
                new Song { Name = "Song5" },
            };

            List<Song> result = DuplicateService.RemoveDuplicateSongs(songs).ToList();

            Assert.IsTrue(result.Count == songs.Count);
            Assert.IsTrue(result[0].Name == songs[0].Name);
            Assert.IsTrue(result[4].Name == songs[4].Name);
        }

        [TestMethod]
        public void RemoveDuplicateSongs_GiveFiveDifferentSongNamesNotInOrder_ExpectFiveSongsOrderNotChanged()
        {
            var songs = new List<Song> {
                new Song { Name = "Song" },
                new Song { Name = "Song5" },
                new Song { Name = "Song3" },
                new Song { Name = "Song4" },
                new Song { Name = "Song2" },
            };

            List<Song> result = DuplicateService.RemoveDuplicateSongs(songs).ToList();

            Assert.IsTrue(result.Count == songs.Count);
            Assert.IsTrue(result[0].Name == songs[0].Name);
            Assert.IsTrue(result[4].Name == songs[4].Name);
        }

        [TestMethod]
        public void RemoveDuplicateSongs_GiveFiveSongsWithTwoNames_ExpectTwoSongs()
        {
            var songs = new List<Song> {
                new Song { Name = "Song" },
                new Song { Name = "Song" },
                new Song { Name = "Song" },
                new Song { Name = "Song2" },
                new Song { Name = "Song2" },
            };

            List<Song> result = DuplicateService.RemoveDuplicateSongs(songs).ToList();

            Assert.IsTrue(result.Count == 2);
            Assert.IsTrue(result[0].Name == songs[0].Name);
            Assert.IsTrue(result[1].Name == songs[4].Name);
        }
    }
}
