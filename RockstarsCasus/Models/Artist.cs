﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RockstarsCasus.Models
{
    public class Artist
    {
        [Key]
        public int ID { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        [Required, Index(IsUnique = true)]
        public string Name { get; set; }

        public ICollection<Song> Songs { get; set; }
    }
}