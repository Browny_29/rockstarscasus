﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RockstarsCasus.Models
{
    public class Song
    {
        [Key]
        public int ID { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        [Required, Index(IsUnique = true)]
        public string Name { get; set; }
        [Required]
        public int Year { get; set; }
        [Required]
        public string Artist { get; set; }
        public Artist ArtistObject { get; set; }
        [Required]
        public string Shortname { get; set; }
        public int? Bpm { get; set; }
        [Required]
        public int Duration { get; set; }
        [Required]
        public string Genre { get; set; }
        public string SpotifyId { get; set; }
        public string Album { get; set; }

    }
}