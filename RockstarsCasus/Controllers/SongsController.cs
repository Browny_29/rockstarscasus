﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RockstarsCasus.Models;
using RockstarsCasus.Services;

namespace RockstarsCasus.Controllers
{
    public class SongsController : ApiController
    {
        private RockstarsCasusContext db = new RockstarsCasusContext();

        // GET: api/Songs
        public IQueryable<Song> GetSongs()
        {
            return db.Songs;
        }

        // GET: api/Songs/5
        [ResponseType(typeof(Song))]
        public async Task<IHttpActionResult> GetSong(int id)
        {
            Song song = await db.Songs.FindAsync(id);
            if (song == null)
            {
                return NotFound();
            }

            return Ok(song);
        }

        // PUT: api/Songs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSong(int id, Song song)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != song.ID)
            {
                return BadRequest();
            }

            db.Entry(song).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SongExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Songs
        [ResponseType(typeof(Song))]
        public async Task<IHttpActionResult> PostSong(ICollection<Song> songs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            songs = DuplicateService.RemoveDuplicateSongs(songs);

            try
            {
                db.Songs.AddRange(songs);
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                if (e.InnerException != null && e.InnerException.InnerException != null && e.InnerException.InnerException.GetType() == typeof(SqlException))
                {
                    BadRequest(e.InnerException.InnerException.Message);
                }
                else throw;
            }
            
            return Ok(songs);

        }

        // DELETE: api/Songs/5
        [ResponseType(typeof(Song))]
        public async Task<IHttpActionResult> DeleteSong(int id)
        {
            Song song = await db.Songs.FindAsync(id);
            if (song == null)
            {
                return NotFound();
            }

            db.Songs.Remove(song);
            await db.SaveChangesAsync();

            return Ok(song);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SongExists(int id)
        {
            return db.Songs.Count(e => e.ID == id) > 0;
        }
    }
}