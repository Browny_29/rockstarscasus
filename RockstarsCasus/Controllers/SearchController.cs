﻿using RockstarsCasus.Interfaces;
using RockstarsCasus.Models;
using RockstarsCasus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace RockstarsCasus.Controllers
{
    public class SearchController : ApiController
    {
        private IContext db = new RockstarsCasusContext();

        /// <summary>
        /// Search for artists based on one of three columns.
        /// </summary>
        /// <param name="input">the input values which will be search upon</param>
        /// <param name="column">the search type. Can be one of three: "name", "genre", "year"</param>
        /// <returns></returns>
        public IHttpActionResult GetSearch(string input, string column)
        {
            if (String.IsNullOrWhiteSpace(input)) return BadRequest("Please give a valid input");
            if (String.IsNullOrWhiteSpace(column)) return BadRequest("Search column string was not recognised.\n\rPlease Enter a correct column");

            switch (column.ToLower())
            {
                case "name":
                    return Ok(SearchService.SearchArtistsByName(input, db));
                case "genre":
                    return Ok(SearchService.SearchArtistsByGenre(input, db));
                case "year":
                    if (int.TryParse(input, out int n))
                        return Ok(SearchService.SearchArtistsByDate(input, db));
                    else return BadRequest("You may only give integer number when searching by year");
                default:
                    return BadRequest("Search column string was not recognised.\n\rPlease Enter a correct column");
            }
        }
    }
}
