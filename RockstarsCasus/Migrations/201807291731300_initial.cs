namespace RockstarsCasus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Artists", "Name", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("dbo.Songs", "Name", c => c.String(nullable: false, maxLength: 100, unicode: false));
            CreateIndex("dbo.Artists", "Name", unique: true);
            CreateIndex("dbo.Songs", "Name", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Songs", new[] { "Name" });
            DropIndex("dbo.Artists", new[] { "Name" });
            AlterColumn("dbo.Songs", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Artists", "Name", c => c.String(nullable: false));
        }
    }
}
