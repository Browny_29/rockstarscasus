namespace RockstarsCasus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullableBPM : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Songs", "SpotifyId", c => c.String());
            AlterColumn("dbo.Songs", "Album", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Songs", "Album", c => c.String(nullable: false));
            AlterColumn("dbo.Songs", "SpotifyId", c => c.String(nullable: false));
        }
    }
}
