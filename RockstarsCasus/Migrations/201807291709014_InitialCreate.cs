namespace RockstarsCasus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Artists",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Songs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Year = c.Int(nullable: false),
                        ArtistName = c.String(nullable: false),
                        Shortname = c.String(nullable: false),
                        Bpm = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        Genre = c.String(nullable: false),
                        SpotifyId = c.String(nullable: false),
                        Album = c.String(nullable: false),
                        Artist_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Artists", t => t.Artist_ID)
                .Index(t => t.Artist_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Songs", "Artist_ID", "dbo.Artists");
            DropIndex("dbo.Songs", new[] { "Artist_ID" });
            DropTable("dbo.Songs");
            DropTable("dbo.Artists");
        }
    }
}
