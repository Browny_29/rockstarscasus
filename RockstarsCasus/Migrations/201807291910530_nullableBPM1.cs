namespace RockstarsCasus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullableBPM1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Songs", "Bpm", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Songs", "Bpm", c => c.Int(nullable: false));
        }
    }
}
