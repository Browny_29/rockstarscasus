namespace RockstarsCasus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class artistchange : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Songs", name: "Artist_ID", newName: "ArtistObject_ID");
            RenameIndex(table: "dbo.Songs", name: "IX_Artist_ID", newName: "IX_ArtistObject_ID");
            AddColumn("dbo.Songs", "Artist", c => c.String(nullable: false));
            DropColumn("dbo.Songs", "ArtistName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Songs", "ArtistName", c => c.String(nullable: false));
            DropColumn("dbo.Songs", "Artist");
            RenameIndex(table: "dbo.Songs", name: "IX_ArtistObject_ID", newName: "IX_Artist_ID");
            RenameColumn(table: "dbo.Songs", name: "ArtistObject_ID", newName: "Artist_ID");
        }
    }
}
