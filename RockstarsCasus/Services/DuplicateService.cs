﻿using RockstarsCasus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RockstarsCasus.Services
{
    public static class DuplicateService
    {
        /// <summary>
        /// Remove song from the collection with duplicate names
        /// </summary>
        /// <param name="songs"></param>
        /// <returns></returns>
        public static ICollection<Song> RemoveDuplicateSongs(ICollection<Song> songs)
        {
            return songs.GroupBy(s => s.Name).Select(g => g.First()).ToList();
        }
    }
}