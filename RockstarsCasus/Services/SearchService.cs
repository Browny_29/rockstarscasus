﻿using RockstarsCasus.Interfaces;
using RockstarsCasus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RockstarsCasus.Services
{
    public static class SearchService
    {
        public static IEnumerable<Artist> SearchArtistsByName(string input, IContext db)
        {
            input = input.ToLower();
            return db.Artists.Where(a => a.Name.ToLower().Contains(input));
        }

        public static IEnumerable<Song> SearchSongsByGenre(string input, IContext db)
        {
            input = input.ToLower();
            return db.Songs.Where(s => s.Genre.ToLower().Contains(input));
        }

        public static IEnumerable<string> SearchArtistsByGenre(string input, IContext db)
        {
            input = input.ToLower();
            var songs = SearchSongsByGenre(input, db);
            return songs.GroupBy(s => s.Artist).Select(g => g.First()).ToList().Select(s => s.Artist);
        }

        public static IEnumerable<string> SearchArtistsByDate(string input, IContext db)
        {
            int intinput = Convert.ToInt32(input.ToLower());
            var songs = db.Songs.Where(s => s.Year <= intinput);
            return songs.GroupBy(s => s.Artist).Select(g => g.FirstOrDefault()).ToList().Select(s => s.Artist);
        }
    }
} 