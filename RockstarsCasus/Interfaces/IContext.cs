﻿using RockstarsCasus.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockstarsCasus.Interfaces
{
    public interface IContext
    {
        DbSet<Artist> Artists { get; set; }

        DbSet<Song> Songs { get; set; }
    }
}
